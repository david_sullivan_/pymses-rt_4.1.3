#!/usr/bin/env python
# -*- coding: utf-8 -*-
#   This file is part of PyMSES.
#
#   PyMSES is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   PyMSES is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with PyMSES.  If not, see <http://www.gnu.org/licenses/>.


# Update version number script from ./setup.py
from datetime import date
# get revision number from .hg/tags.cache file
try:
    try:
        rev_file = open('.hg/tags.cache','r')
    except:
        rev_file = open('.hg/cache/tags','r')
    s = rev_file.read()
    rev_file.close()
    revision_number = s.split(" ")[0]
    print "Updating ./pymses/__init__.py file revision number to " + revision_number
except:
    revision_number = "???"
    print "No mercurial revision number found..."

# get version number from setup.py file
setup_file = open('setup.py','r')
s = setup_file.read()

setup_file.seek(s.find("version"))
spl = setup_file.readline().split("\"")
setup_file.close()
version_number = spl[1]

# update init file
init_file = open('pymses/__init__.py','r+')
s = init_file.read()
spl = s.split("'")
spl[1] = version_number
spl[3] = "$Revision: " + str(revision_number) + "$"
spl[5] = date.today().strftime("$Date: %Y-%m-%d $")
new_s = "'".join(spl)
init_file.seek(0)
init_file.write(new_s)
init_file.truncate()
init_file.close()
