#!/usr/bin/env python
# -*- coding: utf-8 -*-
#   This file is part of PyMSES.
#
#   PyMSES is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   PyMSES is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with PyMSES.  If not, see <http://www.gnu.org/licenses/>.

from numpy.distutils.core import setup
from numpy.distutils.misc_util import Configuration


def make_config(parent_package='', top_path=None):
    config = Configuration(
        package_name="pymses",
        parent_package=parent_package,
        top_path=top_path)

    config.add_extension(
        name="sources.ramses.tree_utils",
        sources=["pymses/sources/ramses/tree_utils.c"])
    config.add_extension(
        name="sources.ramses._octree_utils",
        sources=["pymses/sources/ramses/py_octree_utils.c"])
    config.add_extension(
        name="utils.point_utils",
        sources=["pymses/utils/point_utils.c"])
    config.add_extension(
        name="utils._point_utils",
        sources=["pymses/utils/py_point_utils.c"])
    config.add_extension(
        name="utils._ray_cube_utils",
        sources=["pymses/utils/py_ray_cube_utils.c"])

    config.add_extension(
        name="sources.ramses._read_ramses",
        sources=["pymses/sources/ramses/io/fio.c",
                 "pymses/sources/ramses/io/read_amr.c",
                 "pymses/sources/ramses/io/read_cells.c",
                 "pymses/sources/ramses/io/read_parts.c",
                 "pymses/sources/ramses/io/py_read_ramses.c"])
    config.add_extension(
        name="sources.ramses.hilbert",
        sources=["pymses/sources/ramses/hilbert.c"])
    config.add_extension(
        name="analysis.raytracing._raytrace",
        sources=["pymses/analysis/raytracing/py_raytrace.c"])
    config.add_extension(
        name="analysis.ppv._ray_cast_ppv",
        sources=["pymses/analysis/ppv/py_ray_cast_ppv.c"])

    return config


def setup_pymses():
    setup(
        name="PyMSES",
        version="4.1.1",
        description="Analysis and visualization Python modules for RAMSES",
        classifiers=["Intended Audience :: Science/Research",
                     "License :: OSI Approved :: GNU General Public License (GPL)",
                     "Operating System :: MacOS :: MacOS X",
                     "Operating System :: POSIX :: AIX",
                     "Operating System :: POSIX :: Linux",
                     "Programming Language :: C",
                     "Programming Language :: Python",
                     "Topic :: Scientific/Engineering :: Astrophysics",
                     "Topic :: Scientific/Engineering :: Analysis",
                     "Topic :: Scientific/Engineering :: Visualization",
                     "Topic :: Scientific/Engineering :: Post-processing"],
        keywords='astrophysics visualization amr ramses',
        author="Damien CHAPON, Thomas GUILLET, Marc LABADENS",
        author_email="damien.chapon@cea.fr",
        url="http://irfu.cea.fr/Projets/PYMSES/index.html",
        license="GPL-3",
        configuration=make_config,
    )
    return


if __name__ == '__main__':
    setup_pymses()
