# -*- coding: utf-8 -*-
#   This file is part of PyMSES.
#
#   PyMSES is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   PyMSES is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with PyMSES.  If not, see <http://www.gnu.org/licenses/>.
r"""
:mod:`pymses.etc.rcsetup` --- Runtime changes configuration setup module
------------------------------------------------------------------------

the ~/.pymses/pymsesrc file can be edited to override the default PyMSES configuration parameters. This file is
written in the JSON format

Example
-------

To set seven custom chemical concentrations 'Xi' fields in any AMR datasource read by PyMSES, you may edit your
~/.pymses/pymsesrc file as follows :
{
    "Version": 1,
    "Multiprocessing max. nproc": 8,
    "RAMSES":{
        "ndimensions": 3,
        "amr_field_descr": [
            {"__type__": "scalar_field", "__file_type__": "hydro", "name": "rho", "ivar": 0},
            {"__type__": "vector_field", "__file_type__": "hydro", "name": "vel", "ivars": [1, 2, 3]},
            {"__type__": "vector_field", "__file_type__": "hydro", "name": "Bl", "ivars": [4, 5, 6]},
            {"__type__": "vector_field", "__file_type__": "hydro", "name": "Br", "ivars": [7, 8, 9]},
            {"__type__": "scalar_field", "__file_type__": "hydro", "name": "P", "ivar": 10},
            {"__type__": "multivalued_field", "__file_type__": "hydro", "name": "Xi", "ivar_first": 11, "nb_vars": 7},
            {"__type__": "scalar_field", "__file_type__": "grav", "name": "phi", "ivar": 0},
            {"__type__": "vector_field", "__file_type__": "grav", "name": "g", "ivars": [1, 2, 3]}
        ]
    }
}

"""
from pymses.sources.ramses.field_descr import AmrDataFileFieldDescriptor, RamsesScalar, RamsesVector, RamsesMultiValued
import os
# from shutil import copyfile
import json


def _validate_rcconfig(base_cfg, custom_cfg):
    """
    Update a base configuration dictionary with values from a custom configuration dictionary
    """
    keys = base_cfg.keys()
    for k in custom_cfg.keys():
        if k not in keys:  # Add custom (key, value) pair in default dict
            base_cfg[k] = custom_cfg[k]
        else:  # Same key found in both dict objects
            if type(base_cfg[k]) is dict:
                if type(custom_cfg[k]) is dict:  # recursive call
                    _validate_rcconfig(base_cfg[k], custom_cfg[k])
                else:  # Ignore user value if it is not a dict
                    pass
            elif isinstance(base_cfg[k], RamsesConfiguration):  # Updates Ramses configuration
                base_cfg[k].update(custom_cfg[k])
            else:  # Overriding value in default dict for key k
                base_cfg[k] = custom_cfg[k]


class rcConfiguration(object):
    __instance = None
    _version_key = "Version"
    _multiprocessing_max_nproc_key = "Multiprocessing max. nproc"
    _ramses_key = "RAMSES"
    _pymsesrc_fname = "pymsesrc"
    _dot_pymses_dir = ".pymses"

    def __new__(cls):
        """
        Load the PyMSES runtime changes configuration file. Get the location of the file (JSON format) as follows :
        * `$PWD/pymsesrc`, if it exists
        * `$HOME/.pymses/pymserc`, if it exists
        * Lastly, it takes the `pymses/etc/pymsesrc` as the default configuration file.
        """
        if rcConfiguration.__instance is not None:
            return rcConfiguration.__instance

        rcConfiguration.__instance = super(rcConfiguration, cls).__new__(cls)

        # Default pymsesrc configuration file
        _etc_pymsesrc = rcConfiguration._get_etc_pymsesrc_filepath()

        # Local directory's `pymsesrc` file
        _fname = rcConfiguration._get_local_pymsesrc_filepath()
        if not os.path.isfile(_fname):
            # Look for a '~/.pymses/pymsesrc' file in the user's home directory
            _fname = rcConfiguration._get_user_dot_pymses_pymsesrc_filepath()
            if not os.path.isfile(_fname):
                # Using default `pymses/etc/pymsesrc` configuration file
                # copyfile(_etc_pymsesrc, _fname)
                _fname = None

        rcConfiguration.__instance.__load_json_cfg_file(_fname)
        return rcConfiguration.__instance

    def __load_json_cfg_file(self, filename=None):

        self._user_pymsesrc_dict = {}
        self._pymsesrc_dict = {}
        self._ramses_config = None

        def as_pymsesrc_dict(d):
            if rcConfiguration._multiprocessing_max_nproc_key in d:
                if not isinstance(d[rcConfiguration._multiprocessing_max_nproc_key], int):
                    raise TypeError("'rcConfiguration._multiprocessing_max_nproc_key' value must be an int")
            if rcConfiguration._ramses_key in d:
                self._ramses_config = RamsesConfiguration(d[rcConfiguration._ramses_key])
                del d[rcConfiguration._ramses_key]
                return d
            else:
                return RamsesConfiguration.as_ramses_config_dict(d)

        with open(rcConfiguration._get_etc_pymsesrc_filepath()) as pymsesrc_dict_file:
            self._pymsesrc_dict = json.load(pymsesrc_dict_file, object_hook=as_pymsesrc_dict)

        if filename is not None:
            with open(filename) as default_pymsesrc_dict_file:
                try:
                    self._user_pymsesrc_dict = json.load(default_pymsesrc_dict_file, object_hook=as_pymsesrc_dict)
                except:
                    print "Warning : invalid JSON file format. '%s' file will be ignored and default rc settings " \
                          "will be used. " % filename

            _validate_rcconfig(self._pymsesrc_dict, self._user_pymsesrc_dict)

    @classmethod
    def _get_etc_pymsesrc_filepath(cls):
        """
        Get the system default pymsesrc config file path

        :return: ``string``
            system pymsesrc file path
        """
        _etc_pymsesrc = os.path.join(os.path.dirname(__file__), rcConfiguration._pymsesrc_fname)
        return _etc_pymsesrc

    @classmethod
    def _get_user_dot_pymses_pymsesrc_filepath(cls):
        """
        Get the user ~/.pymses/pymsesrc config file path

        :return: ``string``
            user pymsesrc file path
        """
        # Look for a '.pymses/' directory in the user's home directory
        _home_dir = os.path.expanduser("~")
        _dot_pymses_path = os.path.join(_home_dir, rcConfiguration._dot_pymses_dir)
        if not os.path.isdir(_dot_pymses_path):
            print "Creating '%s' directory into the home directory : '%s'." % (_dot_pymses_path, _home_dir)
            os.makedirs(_dot_pymses_path)  # Create `~/.pymses` directory
        _user_pymsesrc = os.path.join(_dot_pymses_path, rcConfiguration._pymsesrc_fname)
        return _user_pymsesrc

    @classmethod
    def _get_local_pymsesrc_filepath(cls):
        """
        Get the local $PWD/pymsesrc config file path

        :return: ``string``
            local pymsesrc file path
        """
        #  Local directory's `pymsesrc` file
        _local_pymsesrc = os.path.join(os.getcwd(), rcConfiguration._pymsesrc_fname)
        return _local_pymsesrc

    @property
    def Ramses(self):
        """
        Get the Ramses run-level changes configuration settings (:class:`~pymses.etc.rcsetup.RamsesConfiguration``
        instance)
        """
        return self._ramses_config

    @property
    def multiprocessing_max_nproc(self):
        return self._pymsesrc_dict[rcConfiguration._multiprocessing_max_nproc_key]


class RamsesConfiguration(object):
    _amr_field_descr_key = "amr_field_descr"
    _ndimensions_key = "ndimensions"
    _field_type_key = "__type__"
    _filetype_key = "__file_type__"

    @classmethod
    def as_ramses_config_dict(cls, d):
        if RamsesConfiguration._field_type_key in d:
            t = d[RamsesConfiguration._field_type_key]
            if RamsesConfiguration._filetype_key in d:
                p = d[RamsesConfiguration._filetype_key]

                if t == RamsesScalar.field_type():
                    return RamsesScalar(p, d['name'], d['ivar'])
                elif t == RamsesVector.field_type():
                    return RamsesVector(p, d['name'], d['ivars'])
                elif t == RamsesMultiValued.field_type():
                    return RamsesMultiValued(p, d['name'], d['ivar_first'], d['nb_vars'])
        return d

    def __init__(self, ramses_dict):
        self.ramses_config_dict = ramses_dict

        amr_field_list = self.ramses_config_dict.pop(RamsesConfiguration._amr_field_descr_key, None)
        if amr_field_list is None:
            self._amr_field_descr = None
        else:
            self._amr_field_descr = AmrDataFileFieldDescriptor(amr_field_list)

    @property
    def ndims(self):
        """
        Get the number of space dimensions of the Ramses simulation output data

        :rtype : int
        :return: the number of space dimensions within the Ramses output data (or None, if the parameter is not defined)
        """
        return self.ramses_config_dict.get(RamsesConfiguration._ndimensions_key)

    @property
    def amr_fields(self):
        """
        Get the AMR data field descriptor

        :rtype : :class:`~pymses.sources.ramses.field_descf.AmrDataFileFieldDescriptor`
        :return: the AMR data field list  descriptor
        """
        return self._amr_field_descr

    def update(self, user_cfg):
        """
        Updates the current :class:`~pymses.etc.rcsetup.RamsesConfiguration`` object with the parameters of a given
        user-defined :class:`~pymses.etc.rcsetup.RamsesConfiguration`` instance

        :param user_cfg: user-defined :class:`~pymses.etc.rcsetup.RamsesConfiguration`` instance
        """
        _validate_rcconfig(self.ramses_config_dict, user_cfg.ramses_config_dict)
        if not isinstance(user_cfg, RamsesConfiguration):
            return

        # Overrides the AMR field list
        self._amr_field_descr = user_cfg.amr_fields


__all__ = ["rcConfiguration"]
