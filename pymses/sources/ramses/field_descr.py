# -*- coding: utf-8 -*-
#   This file is part of PyMSES.
#
#   PyMSES is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   PyMSES is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with PyMSES.  If not, see <http://www.gnu.org/licenses/>.
r"""
:mod:`pymses.sources.ramses.field_descr` --- RAMSES output files field descriptor package
-----------------------------------------------------------------------------------------

"""
from numpy import unique  # , rollaxis, expand_dims, concatenate
from ..field_types import Scalar, Vector, MultiValued


class _RamsesFilePrefixMixin(object):
    """
    Ramses file prefix mixin
    """
    def _set_prefix(self, file_prefix):
        self._file_prefix = file_prefix

    @property
    def file_prefix(self):
        """
        Ramses file prefix
        """
        return self._file_prefix


class RamsesScalar(Scalar, _RamsesFilePrefixMixin):
    """
    Ramses scalar field descriptor

    Parameters
    ----------

    file_prefix : ``string``
        Ramses output data file prefix
    name : ``string``
        name of the field
    ivar : ``int``
        data file variable index that needs to be read

    Examples
    --------

        >>> rho_field = RamsesScalar("hydro", "rho", 0)
        >>> P_field = RamsesScalar("hydro", "P", 4)

    See also
    --------
    RamsesOutput.define_amr_scalar_field : :meth:`~pymses.sources.ramses.output.RamsesOutput.define_amr_scalar_field`
        lets you define a scalar field into a :class:`~pymses.sources.ramses.output.RamsesOutput` instance.
    """
    def __init__(self, file_prefix, name, ivar):
        super(RamsesScalar, self).__init__(name, ivar)
        self._set_prefix(file_prefix)

    def collect_data(self, data):
        return data[:, :, self._ivars[0]]

    def copy(self):
        """
        Returns
        -------
        f: :class:`~pymses.sources.ramses.field_descr.RamsesScalar`
            copy of the RamsesScalarField
        """
        return RamsesScalar(self._file_prefix, self._name, self._ivars[0])

    def __repr__(self):
        return "%s, file = \"%s\")" % (super(RamsesScalar, self).__repr__()[:-1], self.file_prefix)


class RamsesVector(Vector, _RamsesFilePrefixMixin):
    """
    Ramses vector field descriptor

    Parameters
    ----------

    file_prefix : ``string``
        Ramses output data file prefix
    name : ``string``
        name of the field
    ivars : ``int``
        data file variable index list that needs to be read

    Examples
    --------

        >>> velocity_field = RamsesVector("hydro", "v", [1, 2, 3])

    See also
    --------
    RamsesOutput.define_amr_vector_field : :meth:`~pymses.sources.ramses.output.RamsesOutput.define_amr_vector_field`
        lets you define a vector field into a :class:`~pymses.sources.ramses.output.RamsesOutput` instance.
    """
    def __init__(self, file_prefix, name, ivars):
        super(RamsesVector, self).__init__(name, ivars)
        self._set_prefix(file_prefix)

    def collect_data(self, data):
        # vect = concatenate([expand_dims(data[ivar], axis=2) for ivar in self._ivars], axis=2)
        # return vect
        # vect = rollaxis(data, 0, 2)
        # return vect[:, :, self._ivars]
        # vect = concatenate(rollaxis(data[self.ivars,:,:], 0, 2), axis=2)
        # return vect
        return data[:, :, self._ivars]

    def copy(self):
        """
        Returns
        -------
        f: :class:`~pymses.sources.ramses.field_descr.RamsesVector`
            copy of the RamsesVector
        """
        return RamsesVector(self._file_prefix, self._name, self._ivars[:])

    def __repr__(self):
        return "%s, file = \"%s\")" % (super(RamsesVector, self).__repr__()[:-1], self.file_prefix)


class RamsesMultiValued(MultiValued, _RamsesFilePrefixMixin):
    """
    Ramses multi-valued field descriptor (contiguous variables)

    Parameters
    ----------

    file_prefix : ``string``
        Ramses output data file prefix
    name : ``string``
        name of the field
    ivar_first : ``int``
        data file variable index of the first variable that needs to be read
    nb_vars : ``int``
        number of data file variable that needs to be read

    Examples
    --------

        >>> B_field = RamsesMultiValued("hydro", "B", 4, 6)

    See also
    --------
    RamsesOutput.define_amr_multivalued_field : :meth:`~pymses.sources.ramses.output.RamsesOutput.define_amr_multivalued_field`
        lets you define a multivalued field into a :class:`~pymses.sources.ramses.output.RamsesOutput` instance.
    """
    def __init__(self, file_prefix, name, ivar_first, nb_vars):
        super(RamsesMultiValued, self).__init__(name, ivar_first, nb_vars)
        self._set_prefix(file_prefix)

    def collect_data(self, data):
        # vect = concatenate([expand_dims(data[ivar], axis=2) for ivar in self._ivars], axis=2)
        # return vect
        # vect = rollaxis(data, 0, 2)
        # return vect[:, :, self._ifirst:self._ifirst+self._nb_vars]
        return data[:, :, self._ifirst:self._ifirst + self._nb_vars]

    def copy(self):
        """
        Returns
        -------
        f: :class:`~pymses.sources.ramses.field_descr.RamsesMultiValued`
            copy of the RamsesMultiValued
        """
        return RamsesMultiValued(self._file_prefix, self._name, self._ivars[0], self._ivars[-1] - self._ivars[0] + 1)

    def __repr__(self):
        return "%s, file = \"%s\")" % (super(RamsesMultiValued, self).__repr__()[:-1], self.file_prefix)


class RamsesFileFieldDescriptor(object):
    """
    Generic Ramses output file field descriptor

    Parameters
    ----------
    field_list : ``list``
         :class:`~pymses.sources.field_types.Field` instance list
    """

    def __init__(self, field_list=None):
        self._field_list = []
        self._fields_by_name = {}
        self._fields_by_file = {}

        if field_list is not None:
            for fd in field_list:
                self._add_field(fd)

    @property
    def field_name_list(self):
        """
        Field name list
        """
        return [fd.field_name for fd in self._field_list]

    def __getitem__(self, item):
        fd = self._fields_by_name.get(item)
        if fd is None:
            raise AttributeError("Unknown field '%s'" % item)
        return fd

    def _add_field(self, field):
        """
        Protected method to add a field into the field list

        Parameters
        ----------

        field: Ramses field object
        :raise AttributeError: occurs if the name of the field is already declared into the field list
        """
        name = field.field_name
        if name in self._fields_by_name:
            raise AttributeError("Field '%s' is already declared. Update it or choose a different"
                                 " name for the new field." % name)

        # Gather fields in a file type-base dictionary and a name-based dictionary
        self._field_list.append(field)
        self._fields_by_name[field.field_name] = field
        prefix = field.file_prefix
        if prefix not in self._fields_by_file:  # Init file type field field list
            self._fields_by_file[prefix] = [field]
        else:  # Append field to the file type field list
            self._fields_by_file[prefix].append(field)

    def add_scalar(self, *args, **kwargs):
        """
        Add a new scalar field into the field list

        :param kwargs: keyword argument dict passed along to the `RamsesScalar` constructor
        """
        field = RamsesScalar(*args, **kwargs)
        self._add_field(field)

    def add_vector(self, *args, **kwargs):
        """
        Add a new vector field into the field list

        :param kwargs: keyword argument dict passed along to the `RamsesVector` constructor
        """
        self._add_field(RamsesVector(*args, **kwargs))

    def add_multivalued(self, *args, **kwargs):
        """
        Add a new scalar field into the field list

        :param kwargs: keyword argument dict passed along to the `RamsesMultiValued` constructor
        """
        self._add_field(RamsesMultiValued(*args, **kwargs))

    def remove_field(self, field_name):
        """
        Removes a field with a given name from the field list

        :param field_name: name of the field to remove
        """
        fd = self.__getitem__(field_name)
        self._field_list.remove(fd)
        del self._fields_by_name[field_name]
        flist = self._fields_by_file[fd.file_prefix]
        flist.remove(fd)
        if len(flist) == 0:  # Delete file type field list if empty
            del self._fields_by_file[fd.file_prefix]

    def get_file_prefix(self, field_name):
        """
        Get the file prefix of a given field (described by its field name)

        :param field_name: Name of the required field
        :rtype : string
        :return: The file prefix of the selected field
        """
        fd = self.__getitem__(field_name)
        return fd.file_prefix

    def reset(self):
        """
        Reset the field list

        """
        self._field_list = []
        self._fields_by_name = {}
        self._fields_by_file = {}

    def copy(self):
        """
        Returns
        -------
        c: : class: `~pymses.sources.ramses.field_descr.RamsesFileFieldDescriptor`
            deep copy of itself
        """
        return self.__class__([fd.copy() for fd in self._field_list])

    def __repr__(self):
        return "%s : [%s]" % (self.__class__.__name__, ",\n".join([fd.__repr__() for fd in self._field_list]))

    def _get_ivars_fdescrs_by_file(self):
        """
        Get the (ivars, field) file type-based dictionary

        Returns
        -------
        ivars_descrs_by_file: ``dict``
            a file-type based dictionary containing 2-tuples of the list if the variable indices to read and the
            list of field.
        """
        ivars_descrs_by_file = {}
        for prefix, fds in self._fields_by_file.iteritems():
            ivars_to_read = []
            new_fields = []
            for fd in fds:
                ivars_to_read += fd.variable_indices  # List concatenation of all ivars list of every field

            # Uniquify (+sort) the ivar list
            unique_ivars, rev_indices = unique(ivars_to_read, return_inverse=True)

            # Gather a new field list, each of which has its ivars list reordered according to the unique ivar list
            i = 0
            for fd in fds:
                fd_new = fd.copy()
                j = len(fd.variable_indices)
                fd_new.update_ivars(rev_indices[i:i + j])
                i += j
                new_fields.append(fd_new)

            # Set the (unique ivar array, new field list) tuple for the current Ramses file prefix
            ivars_descrs_by_file[prefix] = ([int(i) for i in unique_ivars], new_fields)

        return ivars_descrs_by_file


class AmrDataFileFieldDescriptor(RamsesFileFieldDescriptor):
    """
    Ramses AMR data file field descriptor

    Parameters
    ----------

    field_list : ``list``
         :class:`~pymses.sources.field_types.Field` instance list
    """
    def __init__(self, field_list=None):
        super(AmrDataFileFieldDescriptor, self).__init__(field_list)

    def gather_read_fields(self, amr_read_fields):
        """
        Get the (ivars, field) amr file type-based dictionary for a set of amr field names to read

        Parameters
        ----------

        amr_read_fields: ``list`` of ``string``
            list of amr field names to read

        Return
        ------
        d : ``dict``
            (ivars, field) amr file type-based dictionary for the required set of amr fields

        """
        descr = AmrDataFileFieldDescriptor([self.__getitem__(fname) for fname in amr_read_fields])
        return descr._get_ivars_fdescrs_by_file()


__all__ = ["RamsesScalar", "RamsesVector", "RamsesMultiValued", "AmrDataFileFieldDescriptor"]
